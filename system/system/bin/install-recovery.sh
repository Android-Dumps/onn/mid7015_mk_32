#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery:12882848:63d2e5bccf6e4b3e424dce966c2e08829a77a494; then
  applypatch  EMMC:/dev/block/platform/bootdevice/by-name/boot:7207840:cab1556bbef2f3807adb0f51910d0d7e5f5cf9ae EMMC:/dev/block/platform/bootdevice/by-name/recovery 63d2e5bccf6e4b3e424dce966c2e08829a77a494 12882848 cab1556bbef2f3807adb0f51910d0d7e5f5cf9ae:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
