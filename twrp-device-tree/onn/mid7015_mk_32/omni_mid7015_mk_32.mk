#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from mid7015_mk_32 device
$(call inherit-product, device/onn/mid7015_mk_32/device.mk)

PRODUCT_DEVICE := mid7015_mk_32
PRODUCT_NAME := omni_mid7015_mk_32
PRODUCT_BRAND := ONN
PRODUCT_MODEL := 100005206
PRODUCT_MANUFACTURER := onn

PRODUCT_GMS_CLIENTID_BASE := android-digiland

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="full_mid7015_mk_32-user 9 PPR1.180610.011 eng.fuqian.20191023.144734 release-keys"

BUILD_FINGERPRINT := ONN/100005206/mid7015_mk_32:9/PPR1.180610.011/kanghua10231432:user/release-keys
