#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_mid7015_mk_32.mk

COMMON_LUNCH_CHOICES := \
    omni_mid7015_mk_32-user \
    omni_mid7015_mk_32-userdebug \
    omni_mid7015_mk_32-eng
